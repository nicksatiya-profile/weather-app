'use strict'

const request = require('request');

const geocode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + encodeURIComponent(address) + '.json?access_token=pk.eyJ1Ijoibmlja3NhdHoiLCJhIjoiY2t0N2dxZGo0MHM2NzJwbzdjbWFwNWFlOCJ9.XPiWIt6JGNOy3FGxRIThPA&limit=1'

    request({ url: url, json: true }, (error, response) => {
        let latitude;
        let longitude;

        if (error) {
            callback('Unable to connect to location service!', undefined); // Either error or response will always be undefined
        }
        else if (response.body.features.length === 0) {
            callback('Unable to find location. Try another search', undefined);
        }
        else {

            callback(undefined, {
                latitude: response.body.features[0].center[1],
                longitude: response.body.features[0].center[0],
                location: response.body.features[0].place_name,
            });
        }
    })

}


module.exports = geocode;