'use strict'

const request = require('request');

const forecast = (latitude, longitude, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=d9cd19e0e17cb59a3dacc320824bb220&query='
        + latitude + ',' + longitude;

    request({ url: url, json: true }, (error, response) => {
        if (error) {
            callback('Unable to connect to weather api!', undefined);
        }
        else if (response.body.error) {
            callback(response.body.error, undefined);
        }
        else {
            callback(undefined, `It is ${response.body.current.weather_descriptions[0]} and currently ${response.body.current.temperature} degrees out.It feels like ${response.body.current.feelslike} degrees out`);
        }
    });
}

module.exports = forecast;