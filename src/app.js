'use strict'

const path = require('path');
const express = require('express');
const hbs = require('hbs');
const geocode = require('../utils/geocode');
const forecast = require('../utils/forecast');
const app = express();
const port = process.env.PORT || 3000; // Heroku port or our local port

// Defined paths for Express config
const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

// Setup handlebars engine and views location
app.set('view engine', 'hbs');
app.set('views', viewsPath);

hbs.registerPartials(partialsPath);

// Setup static directory to serve
app.use(express.static(publicDirectoryPath));

// Routes
app.get('/', (req, res) => {

    res.render('index', {
        title: 'Weather',
    });
});

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'You must provide an address!'
        });
    }
    geocode(req.query.address, (error, data) => {

        if (error) {
            return res.send({
                error: error
            });
        }
        forecast(data.latitude, data.longitude, (error, forecastData = {}) => {
            if (error) {
                return res.send({
                    error: error
                });
            }
            else {
                return res.send({
                    forecast: `${forecastData}`,
                    location: data.location,
                    address: req.query.address,
                });
            }
        })
    })

})



app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help Page',
        helpText: 'This is some help text'
    });
})


app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About Page'
    });
});

// 404 Routes

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: 'Error 404',
        error: 'Help article does not exist!',
    });
});

app.get('*', (req, res) => {
    res.render('404', {
        title: 'Error 404',
        error: "Error 404! Page doesn't exits",
    });
});



app.listen(port, () => {
    console.log('Server is up on port ' + port);
});
